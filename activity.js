db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $count: "name" }

]);

db.fruits.aggregate([
    {$match: {stock: {
          			$gt: 20} } },
    {
      $count: "stock"
    }
  ]);

db.fruits.aggregate([
	{ $match: {onSale: true}},
    { $group: { _id: "$suppier_id", avgAmount: { $avg: { $multiply: [ "$price", "$stock"] } } } }
]);

db.fruits.aggregate([
     {$group:{_id: "$suppier_id",maxTotalAmount: { $max: { $multiply: [ "$price", "$stock"] } } } }
]);

db.fruits.aggregate([
     { $group:
         {
           _id: "$suppier_id",
           minTotalAmount: { $min: "$price" } } }
]);

